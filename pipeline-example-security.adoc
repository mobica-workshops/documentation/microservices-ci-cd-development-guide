We are having in out pipeline security docker scan test.

[source,yaml,linenos]
----
.trivy_scan_definition:
  image:
    name: docker.io/aquasec/trivy:latest
    entrypoint: [ "" ]
  variables:
    # No need to clone the repo, we exclusively work on artifacts.  See
    # https://docs.gitlab.com/ee/ci/runners/README.html#git-strategy
    GIT_STRATEGY: none
    TRIVY_USERNAME: "$CI_REGISTRY_USER"
    TRIVY_PASSWORD: "$CI_REGISTRY_PASSWORD"
    TRIVY_AUTH_URL: "$CI_REGISTRY"
    CONTAINER_TEST_IMAGE: "${CI_REGISTRY_IMAGE}/test:${CI_COMMIT_SHA}_${CI_PIPELINE_ID}"
  script:
    - trivy --version
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy image --clear-cache
    # update vulnerabilities db
    - time trivy image --download-db-only --no-progress --cache-dir .trivycache/
    # Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    - time trivy image --exit-code 0 --cache-dir .trivycache/ --no-progress --format template --template "@/contrib/gitlab.tpl"
        --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$CONTAINER_TEST_IMAGE"
    # Prints full report
    - time trivy image --exit-code 0 --cache-dir .trivycache/ --severity CRITICAL,HIGH,MEDIUM --ignore-unfixed --no-progress "$CONTAINER_TEST_IMAGE"
    # Fail on critical vulnerabilities which are fixed
    - time trivy image --exit-code 1 --cache-dir .trivycache/ --severity CRITICAL --ignore-unfixed --no-progress "$CONTAINER_TEST_IMAGE"
  allow_failure: true
  cache:
    paths:
      - .trivycache/
  # Enables https://docs.gitlab.com/ee/user/application_security/container_scanning/ (Container Scanning report is available on GitLab EE Ultimate or GitLab.com Gold)
  artifacts:<.>
    when:                          always
    reports:
      container_scanning:          gl-container-scanning-report.json
----
<.> unfortunately this report we are saving as artifact is only available in the paid plan.

We are having some example scans done here. You can check what just by read script section.