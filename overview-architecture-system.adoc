In the end our full architecture on the staging and production environment looks like this:

ifdef::backend-html5[]
.Target System Architecture
[#img-system]
[caption="Figure 3: "]
image::images/system.svg[Target Architecture]
endif::[]
ifndef::backend-html5[]
.Target System Architecture
[#img-system]
[caption="Figure 3: "]
image::images/system.png[Target Architecture]
endif::[]

- Our FrontEnd Service will be simulated with https://gitlab.com/mobica-workshops/examples/js/vuejs/book-frontend[Book Frontend].

In the upper target system which will be available on the staging and production environments our frontend is talking through the Ingres with our BFF Service which is operating in the fully working environment. We will be deploying our services with the help of the CI/CD Pipeline to those environments.

Locally we will be working with the mocked vwrsion of this architecture which will be looking like this:

ifdef::backend-html5[]
.Local Mocked System Architecture
[#img-system-mockups]
[caption="Figure 4: "]
image::images/system-mocked.svg[Mocked Architecture]
endif::[]
ifndef::backend-html5[]
.Local Mocked System Architecture
[#img-system-mockups]
[caption="Figure 4: "]
image::images/system-mocked.png[Mocked Architecture]
endif::[]
