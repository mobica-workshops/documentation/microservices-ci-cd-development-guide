ifdef::backend-html5[]
**TODO:** Video with demo
endif::[]

Now Let's start with the `.gitlab-ci.yml` file which we have in the https://gitlab.com/mobica-workshops/examples/js/vuejs/book-frontend/[Book Frontend] repository

[source,yaml,linenos]
----
# Version 5.0
variables:
  APP_NAME: book-frontend <.>

include: <.>
  - project: 'mobica-workshops/templates/ci/gitlab-ci' <.>
    file: <.>
      - 'js.vuejs.pipeline.v5.yml' <.>
----
<.> environment variable which can be used in the pipeline jobs
<.> we can include in our yaml file other yaml files which is super important as grants us possibility to reuse code.
<.> in our case we are informing Gitlab about which project in the gitlab server will have file or files we plan to include
<.> in this section we will be informing Gitlab CI about those files
<.> in the our case we are including one file which is de-facto our default pipeline for the vuejs based projects

Like you can see include is very powerful as Gitlab will merge all files into one based on those includes what prevents a lot of code duplication and also makes possible to update pipelines for a multiple projects at once. We can also have include used in the files we are including.

There are also cons with it like your pipelines will be more complex and harder to read. Here it is why Gitlab build in ci editor is so important as it will give you possibility to analyze your full pipeline - merging all this includes to show you graph of it. Other con is that if you will make mistake in the shared pipeline all pipelines which are based on it will be broken.