Each project in the Gitlab system have a Project menu:

.Gitlab - project menu
[#img-gitlab-overview-project]
[caption="Figure 1: "]
image::images/gitlab-book-frontend-menu.png[Gitlab - project menu]

This menu is giving us access to all Gitlab features which are project related.
