We have a **Pinned** section which is configurable and have shortcuts to the feature which normally are the most used. By default it is **Issues** and **Merge requests**.
