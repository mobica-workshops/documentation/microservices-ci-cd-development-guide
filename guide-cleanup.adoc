Let's start with removing our deployment from the cluster. Please add new job called `cleanup`

[source,yaml]
----
cleanup:
  stage: deploy
  variables:
    PROJ_ENV: staging
  image: alpine/k8s:1.27.4
  before_script:
    - if [ $PROJ_ENV == "production" ]; then export KUBECONFIG=$KUBE_CONFIG_PROD ; else export KUBECONFIG=$KUBE_CONFIG_STAG ; fi
  script:
    - helm delete --namespace="workshops-${PROJ_ENV}" "${PROJ_ENV}-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
  only:
    - master
  when: manual
----

Commit changes wait till it will go to the manual jobs and run them one after another, and insterad of the `staging` please choose `cleanup`.

In the `cleanup` job in our current pipeline we should see this:

[source,text,linenos]
----
Running with gitlab-runner 16.3.0 (8ec04662)
  on gitlab-runner-bf79f759d-9xgcm k9s1JGXC, system ID: r_TNb3aMOHeJHC
....
$ helm delete --namespace="workshops-${PROJ_ENV}" "${PROJ_ENV}-${CI_PROJECT_NAME}-${CI_PROJECT_ID}"
...
release "staging-book-frontend-50493637" uninstalled <.>
Cleaning up project directory and file based variables 00:00
Job succeeded
----
<.> You should see that your release was uninstalled.

