Like was mentioned upper we can extend our jobs with reusable jobs definitions.

Let's start with a job which we called `yarn_install` end was extended with `.vue_configure_job_definition` located in the `.vue-ci-template.yml` file.

[source,yaml,linenos]
----
.vue_configure_job_definition:
  image: node:18 <.>
  script:
    - yarn install <.>
  artifacts: <.>
    expire_in: 1 week
    paths:
      - node_modules
----
<.> This job will use `node:18` as a docker image for runner
<.> We will install all dependencies
<.> We will store installed dependencies as artifacts for 1 week and use them in all other jobs which will depend on this one.

We of course have also a build job:

[source,yaml,linenos]
----
.vue_build_job_definition:
  image: node:18
  script:
    - yarn build
  artifacts: <.>
    expire_in: 1 week
    paths:
      - dist
----
<.> This job will have artifacts also stored as an build will create them in the dist folder. In our case it will not be used as we have deployment based on the docker but in many projects those artifacts can be deployed by example to the S3 bucket in the AWS Cloud and used.

Other job we have here is one which is responsible for the Unit Testing

[source,yaml,linenos]
----
.vue_ut_job_definition:
  image: node:18
  script:
    - yarn coverage
  artifacts:
    reports: <.>
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml
----
<.> This one will end with generating coverage report. In gitlab there is visualisation of those reports which is better in the paid plans but also exists in the free plan. For the free plan we need to use coverage in the cobertura format.

We also have our other jobs we are using for our VueJs pipelines

[source,yaml,linenos]
----
.vue_test_prettier_job_definition:
  image: node:18
  script:
    - yarn check

.vue_test_eslint_job_definition:
  image: node:18
  script:
    - yarn lint
----
