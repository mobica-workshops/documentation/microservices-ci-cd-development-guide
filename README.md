# Developing microservices - CI/CD

## Checklist v1.2

- [ ] Azure DevOps infrastructure
- ... to be specified later

## Checklist v1.1

- [ ] add staticcheck, revive, golangci-lint linter use examples (go vet and staticcheck in the pipeline)
- [ ] add govulncheck for security examples
- [ ] add HTTP load generator hey use example
- [ ] Adding Release example for the Gitlab examples
- [ ] GitHub Actions runners/workers infrastructure
- [ ] backend microservice book-list GitHub Actions pipeline
- [ ] backend microservice book-admin GitHub Actions pipeline
- [ ] backend microservice book-bff GitHub Actions pipeline
- [ ] frontend microservice book-frontend GitHub Actions pipeline
- [ ] backend microservice GitHub Actions pipeline example - explanations ... to be specified later
- [ ] frontend microservice GitHub Actions pipeline hands-on training (test, build, release, deploy)

## Checklist v1.0

- [x] GCP core infrastructure
- [x] GKE cluster
- [x] Gitlab runners infrastructure
- [x] Staging Environment infrastructure
- [x] Production Environment infrastructure
- [x] Deployment setup for the kubernetes deployment process
- [x] backend microservice book-list GitlabCI pipeline - v5 version
- [x] backend microservice book-admin GitlabCI pipeline - v5 version
- [x] backend microservice book-bff GitlabCI pipeline - v5 version
- [x] frontend microservice book-frontend GitlabCI pipeline
- [x] frontend microservice GitlabCI architecture overview
- [x] frontend microservice GitlabCI pipeline detailed overview
- [x] backend microservices GitlabCI pipeline short overview
- [x] Gitlab project menu overview
- [x] Gitlab Container Registry overview
- [x] Gitlab Pages overview
- [x] Gitlab CI overview 
- [x] Official Gitlab tutorial: Create and run your first GitLab CI/CD pipeline
- [x] Official Gitlab tutorial: Create a complex pipeline
- [x] frontend microservice GitlabCI pipeline example - base template
- [x] frontend microservice GitlabCI pipeline example - vuejs extending template 
- [x] frontend microservice GitlabCI pipeline example - configuration section explanation
- [x] frontend microservice GitlabCI pipeline example - pre-build testing section explanation
- [x] frontend microservice GitlabCI pipeline example - build section explanation
- [x] frontend microservice GitlabCI pipeline example - test section explanation
- [x] frontend microservice GitlabCI pipeline example - release section explanation
- [x] frontend microservice GitlabCI pipeline example - security test explanation
- [x] frontend microservice GitlabCI pipeline example - deploy section explanation
- [x] backends microservices GitlabCI pipelines changes  
- [x] frontend microservice book-frontend archive without pipeline for hands-on training
- [x] fully working frontend microservice book-frontend example
- [x] frontend microservice GitlabCI pipeline hands-on training (configure, test, release, deploy)
- [x] cleanup
