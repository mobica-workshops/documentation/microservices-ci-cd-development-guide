[.lead]
During this hands on session you will learn how to prepare pipeline which will start with Continuous Integration process, then continue with Continuous Delivery process and will finally end with the Continuous Deployment process.

[WARNING]
====
Normally when working with pipeline you should check if it will fail if something will go wrong. Unfortunately we do not have enough time to practice this during this workshop - please play with your pipeline later after training
====

ifdef::backend-html5[]
please download link:images/book-frontend.tar/[book-frontend.tar]
endif::[]
ifndef::backend-html5[]
please create book-frontend.tar by cloning https://gitlab.com/mobica-workshops/students/arkadiusz-tulodziecki-mobica/book-frontend repository and running in it command: `git archive --format=tar -o book-frontend.tar clean-project`
endif::[]

Please create a public project called `book-frontend` and push to it all files you have inside this `book-frontend.tar` file.

