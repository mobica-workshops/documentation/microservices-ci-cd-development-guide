We will be using **Gitlab Pages** feature to deploy our project technical documentation.

Each of our example projects have technical documentation created with a use of SSG (Static Site Generator) software. We are using asciidoctor in our projects byt there are better ones to do this like for example Hugofootnote:[Hugo - The world’s fastest framework for building websites https://gohugo.io/] which is used by example to create Kubernetes documentation.

For example, you can check link to the Documentation we are creating using our pipeline for the Book Frontend service in the https://gitlab.com/mobica-workshops/examples/js/vuejs/book-frontend/pages[Pages] subsection in the **Deploy** section.

In our case documentation link looks like this: https://mobica-workshops.gitlab.io/examples/js/vuejs/book-frontend/
