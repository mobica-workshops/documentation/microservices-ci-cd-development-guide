#!/usr/bin/env bash
mkdir public
cp -R images public/
cp microservices-ci-cd-development-guide.html public/index.html
cp microservices-ci-cd-development-guide.pdf public/
