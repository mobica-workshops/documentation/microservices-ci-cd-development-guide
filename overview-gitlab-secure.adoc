We have **Secure** section where we have these features:

- Security capabilities
- Audit events
- Security configuration

Those features are available mostly in the paid plans of the Gitlab system.
