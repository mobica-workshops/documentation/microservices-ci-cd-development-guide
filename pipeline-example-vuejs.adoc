Because our **Book Frontend** application is including default pipeline we prepared for our vuejs based projects we need to check it. We are storing all default pipelines and fragments we will be discussing later in the repository https://gitlab.com/mobica-workshops/templates/ci/gitlab-ci[Gitlab CI Templates]

Let's analyze it block by block to better understand how it works.

[source,yaml,linenos]
----
include:
  - project: 'mobica-workshops/templates/ci/gitlab-ci'
    file:
      - 'default.pipeline.v5.yml' <.>
      - '.vue-ci-template.yml' <.>

----
<.> Looks like this pipeline in our case extends another pipeline called **default**. In the bigger projects when we can have microservices created in many languages we can have a default pipeline which will be extended with jobs and sometimes even stages which are language based
<.> We have also a file which is a language based here as our pipelines are complex and we prefer to be them as readable as possible

Our VueJs pipeline extends Default pipeline **configure** stage like this:

[source,yaml,linenos]
----
###############################################
# Configure stage
###############################################
# Generate node modules
yarn_install:
  stage: configure
  extends: .vue_configure_job_definition <.>
----
<.> The same as we can include full yaml files which will be merged into one complete pipeline we can reuse code even more with **extends** which will only extend job with a reusable part. We will look at this job definition later.

We are also adding our language based **pre-build-tests**

[source,yaml,linenos]
----
###############################################
# pre-build-tests stage
###############################################
# Run code style checker
code_format_check:
  extends:
    .vue_test_prettier_job_definition <.>
  stage: pre-build-tests
  dependencies:
    - yarn_install
    - configure

# Run code style checker
static_analyse:
  extends:
    .vue_test_eslint_job_definition <.>
  stage: pre-build-tests
  dependencies:
    - yarn_install
    - configure

# Run UT tests with coverage enabled
unit_tests:
  extends:
    .vue_ut_job_definition <.>
  stage: pre-build-tests
  dependencies: <.>
    - yarn_install
    - configure
----
<.> We are using prettier for a code format check
<.> We are using eslint for a static code analysis
<.> Our app also have unit tests prepared
<.> It is good to have dependencies in this place but as you can see sometimes you need to give dependency which is in the other pipeline file. In our case **configure** job exists in the **default** pipeline

We are extending also a **build** stage:
[source,yaml,linenos]
----
###############################################
# Build stage
###############################################
# Build application
build_package:
  extends:
    .vue_build_job_definition
  stage: build
  dependencies:
    - configure
    - yarn_install
    - code_format_check
    - static_analyse
    - unit_tests
----
